package com.tistory.blackrug.deoginapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by wogjs_000 on 2015-12-21.
 */
public class BapActivity extends AppCompatActivity{

    ListView mListView;
    BapListAdapter mAdapter;
    BapDownloadTask mProcessTask;
    ProgressDialog mDialog;
    Calendar mCalendar;

    boolean isUpdating = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bap);
        mCalendar = Calendar.getInstance();
        mListView = (ListView) findViewById(R.id.mListView);
        mAdapter = new BapListAdapter(getApplicationContext());
        mListView.setAdapter(mAdapter);


        getBapList(true);
    }



    private void getBapList(boolean isUpdate) {
        mAdapter.clearData();
        mAdapter.notifyDataSetChanged();

        final Calendar mToday = Calendar.getInstance();
        final int TodayYear = mToday.get(Calendar.YEAR);
        final int TodayMonth = mToday.get(Calendar.MONTH);
        final int TodayDay = mToday.get(Calendar.DAY_OF_MONTH);

        if (mCalendar == null)
            mCalendar = Calendar.getInstance();

        mCalendar.add(Calendar.DATE, 2 - mCalendar.get(Calendar.DAY_OF_WEEK));

        for (int i = 0; i < 6; i++) {
            int year = mCalendar.get(Calendar.YEAR);
            int month = mCalendar.get(Calendar.MONTH);
            int day = mCalendar.get(Calendar.DAY_OF_MONTH);

            BapTool.restoreBapDateClass mData =
                    BapTool.restoreBapData(getApplicationContext(), year, month, day);

            if (mData.isBlankDay) {
                if (Tools.isOnline(getApplicationContext())) {
                    if (!isUpdating && isUpdate) {
                        mDialog = new ProgressDialog(this);
                        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                        mDialog.setMax(100);
                        mDialog.setTitle(R.string.loading_title);
                        mDialog.setCancelable(false);
                        mDialog.show();

                        mProcessTask = new BapDownloadTask(getApplicationContext());
                        mProcessTask.execute(year, month, day);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.no_network_msg, Toast.LENGTH_SHORT).show();
                }

                return;
            }
            if ((year == TodayYear) && (month == TodayMonth) && (day == TodayDay)) {
                mAdapter.addItem(mData.Calender, mData.DayOfTheWeek, mData.Breakfast, mData.Lunch, mData.Dinner);
            } else {
                mAdapter.addItem(mData.Calender, mData.DayOfTheWeek, mData.Breakfast, mData.Lunch, mData.Dinner);
            }
            mCalendar.add(Calendar.DATE, 1);
        }

        mAdapter.notifyDataSetChanged();
        setCurrentItem();
    }

    private void setCurrentItem() {
        int DAY_OF_WEEK = mCalendar.get(Calendar.DAY_OF_WEEK);

        if (DAY_OF_WEEK > 1 && DAY_OF_WEEK < 7) {
            mListView.setSelection(DAY_OF_WEEK - 2);
        } else {
            mListView.setSelection(0);
        }
    }

    public class BapDownloadTask extends ProcessTask {
        public BapDownloadTask(Context mContext) {
            super(mContext);
        }

        @Override
        public void onPreDownload() {
            isUpdating = true;
        }

        @Override
        public void onUpdate(int progress) {
            mDialog.setProgress(progress);
        }

        @Override
        public void onFinish(long result) {
            if (mDialog != null)
                mDialog.dismiss();

            isUpdating = false;

            if (result == -1) {
                return;
            }

            getBapList(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mDialog != null)
            mDialog.dismiss();

        mCalendar = null;
    }
}